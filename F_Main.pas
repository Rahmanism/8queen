unit F_Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, ChessBrd;

type
  TMain = class(TForm)
    ChessBrd1: TChessBrd;
    Label1: TLabel;
    Edit: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormShow(Sender: TObject);
    procedure EditChange(Sender: TObject);
  private
    Procedure QueenProblem;
    procedure Queen8(x:byte);
    function  test(x,y: byte): boolean;
    procedure output;
  public

  end;

var
  Main: TMain;

implementation
uses  ShellAPI;

{$R *.dfm}

var
  num:Integer;
  field:array[1..8,1..8] of boolean;
  lsg: word;

function Indent(ACount: integer): string;
begin
  Result:='';
  while ACount > 0 do begin
    Result := Result + '  ';
    dec(ACount);
  end;
end;

function TMain.test(x,y: byte): boolean;
var
  a: byte;
begin
  result:=false;
  if x > 1 then
    for a := 1 to x-1 do
      if field[x-a,y] or (y-a >= 1) and field[x-a,y-a] or(y+a <= 8) and field[x-a,y+a] then
        exit;
  Result:=true;
end;

procedure TMain.output;
var
  x,y,k : byte;
  st:String;
begin
  st:=Indent(63);
  for y := 1 to 8 do
    for x := 1 to 8 do
    begin
      k:=((y-1)*8)+x;
      if field[x,y] then
        st[k]:='Q'
      else
        st[k]:=' ';
    end;
  main.ChessBrd1.Position:=st;
end;

procedure TMain.Queen8(x:byte);
var
  y,i : byte;
begin
  for y := 1 to 8 do
  begin
    if test(x,y) then
    begin
      field[x,y]:=true;
      if x < 8 then
        Queen8(x+1)
      else
      begin
        num:=num+1;
        if num=main.Edit.ItemIndex+1 then
          output;
      end;
    end;
    for i:=1 to 8 do
      field[x,i]:=false;
  end;
end;

Procedure TMain.QueenProblem;
var
  x,y:byte;
begin
  for y := 1 to 8 do
    for x := 1 to 8 do
      field[x,y]:=false;
  lsg:=0;
  num:=0;
  Queen8(1);
end;

procedure TMain.FormShow(Sender: TObject);
var
  i:Integer;
begin
  for i:=1 to 92 do
    edit.Items.Add(Inttostr(i));
  edit.ItemIndex:=0;   
  QueenProblem;
end;

procedure TMain.EditChange(Sender: TObject);
begin
  QueenProblem;
end;

end.
