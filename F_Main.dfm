object Main: TMain
  Left = 236
  Top = 175
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Eight Queens'
  ClientHeight = 422
  ClientWidth = 400
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ChessBrd1: TChessBrd
    Left = 8
    Top = 6
    Width = 392
    Height = 392
    AnimateMoves = False
    AnimationDelay = 0
    BoardLines = True
    BorderColor = clBlack
    CastlingAllowed = []
    ComputerPlaysBlack = False
    ComputerPlaysWhite = False
    Thinking = False
    CoordFont.Charset = DEFAULT_CHARSET
    CoordFont.Color = clWhite
    CoordFont.Height = -9
    CoordFont.Name = 'Arial'
    CoordFont.Style = []
    CurrentMove = 1
    DisplayCoords = [West, North]
    CustomEngine = True
    EnPassant = H3
    Position = '                                                               '
    Resizable = False
    ResizeMinSize = 100
    ResizeMaxSize = 1000
    SearchDepth = 1
    SizeOfBorder = 24
    SizeOfSquare = 43
    SquareColorDark = clGray
    SquareColorLight = clSilver
    WhiteOnTop = False
    WhiteToMove = False
    ThinkingPriority = tpNormal
  end
  object Label1: TLabel
    Left = 8
    Top = 384
    Width = 87
    Height = 13
    Caption = #1605#1581#1605#1583#1585#1590#1575' '#1606#1608#1585#1608#1586#1610
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 400
    Width = 105
    Height = 13
    Caption = #1587#1610#1583#1575#1581#1587#1575#1606' '#1581#1587#1610#1606#1610
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 283
    Top = 384
    Width = 111
    Height = 13
    Caption = #1575#1587#1578#1575#1583': '#1582#1575#1606#1605' '#1583#1603#1578#1585' '#1578#1583#1610#1606
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 277
    Top = 400
    Width = 117
    Height = 13
    Caption = #1575#1587#1578#1575#1583': '#1570#1602#1575#1610' '#1583#1603#1578#1585' '#1606#1610#1575#1586#1610
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Edit: TComboBox
    Left = 165
    Top = 387
    Width = 69
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = EditChange
  end
end
